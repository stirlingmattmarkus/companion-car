#define REVERSE -90
#define DIST_CAP 80 // distance in cm where the car will stop following an object
#define MIN_SPEED 20

int US_left[] = {7, 6};    // {trig pin, echo pin}
int US_right[] = {4, 3};

// variables for alternative sensing
bool sensingRight = true; // which sensor is active
long lastSensorSwitch = 0;
int switchDelay = 40; // time to wait until the other sensor takes a reading

const int leftMotorPin = 9;
const int rightMotorPin = 10;
const int reversePin = 11;

const int redLedPin = 13;
const int greenLedPin = 12;
long lastBlinkTime = 0; // time in ms when last blink occured
int blinkDelay = 500;
bool blinkOn = false;

float carSpeed = 0; 
int turn = 0; // number between -100 and +100. Negative means turn right.
float carAcc = 0;

float distanceLeft = 0;   // distance read by left US sensor
float distanceRight = 0;  // distance read by right US sensor

float distanceLeft_arr[10] = {};
float distanceRight_arr[10] = {};

volatile bool interruptHappened = false;  // variable changed via interrupt. MUST BE VOLATILE
bool carIsActive = false;  

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 300;    // the debounce time; increase if the output flickers

// variables for controlling the rate that speed is updated
long lastSpeedUpdate = 0;
int FPS = 60;
int speedUpdateDelay = 1000 / FPS; // Number of times a second that speed is updated

// Variables will change:
int ledState = HIGH; // the current state of the output pin
int lastLedState = LOW;
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin


// SETUP
void setup() {
    pinMode(US_left[0], OUTPUT);  // trigger pin
    pinMode(US_left[1], INPUT);   // echo pin
    pinMode(US_right[0], OUTPUT);
    pinMode(US_right[1], INPUT);
    
    pinMode(leftMotorPin, OUTPUT);
    pinMode(rightMotorPin, OUTPUT);
    pinMode(reversePin, OUTPUT);
    
    pinMode(redLedPin, OUTPUT);
    pinMode(greenLedPin, OUTPUT);
    
    attachInterrupt(digitalPinToInterrupt(2), ISR_button, RISING);  // attach interrupt function (ISR_button) to pin 2
    
    for (int i = 0; i < 10; i++){
        distanceLeft_arr[i] = i;
        distanceRight_arr[i] = i;
    }
    
    Serial.begin(9600);
    //carIsActive = true;
}

int dir = 100;

void loop(){
    loop1();
}

// LOOP
void loop1() {
    
    if (carIsActive) {
        if ( (millis() - lastSensorSwitch) > switchDelay){
            if (sensingRight){
                distanceRight = getDistance(US_right);
                if (distanceRight > DIST_CAP) distanceRight = DIST_CAP;
                addToFloatArray(distanceRight, distanceRight_arr, 10);
            } else {
                distanceLeft = getDistance(US_left);
                if (distanceLeft > DIST_CAP) distanceLeft = DIST_CAP;
                addToFloatArray(distanceLeft, distanceLeft_arr, 10);
            }
            sensingRight = (sensingRight == false);
            lastSensorSwitch = millis();
        }
        //carSpeed = 150;
        if ( (millis() - lastSpeedUpdate) > speedUpdateDelay) {
            updateSpeed(distanceLeft, distanceRight);
            //updateSpeedImproved(distanceLeft_arr, distanceRight_arr);
            lastSpeedUpdate = millis();
        }
    }

    
    updateMotors(carSpeed, turn);
    updateLeds();
    handleInterrupt();

    if (true){
        if (millis()%100 == 0){
            const float aveDist = (float)(distanceLeft + distanceRight) / 2;
            Serial.print(distanceLeft);
            Serial.print(" ");
            Serial.print(distanceRight);
            Serial.print(" ");
            for (int i = 0; i < 10; i++) {
                //Serial.print(distanceLeft_arr[i]);
                //Serial.print(" ");
            }
            Serial.print(carSpeed);
            int index = 3;
            float delta_t = (float)(index * switchDelay * 2) / 1000;
            float diff = distanceLeft - distanceLeft_arr[index];
            float v = diff / delta_t;
            Serial.print(" ");
            //Serial.print(diff);
            Serial.print(" ");
            //Serial.print();
            Serial.print(" ");
            //Serial.print(distanceRight - distanceRight_arr[index]);
            Serial.print(" ");
            Serial.println();
        }
    }

}


//// CAR CONTROL FUNCTIONS ////

// function takes distance from left and right US sensors
// and decides how carSpeed and turn should be modified
void updateSpeed(int leftDist, int rightDist){

    const float minDist = min(leftDist, rightDist);
    const float maxDist = max(leftDist, rightDist);

    int topSpeed = 120;

    float stopThreshold_upper = 40;
    float stopThreshold_lower = 2;
    
    float targetDist = 8;
    float innerThresh = 2;
    float outerThresh = 6;

    float diff = minDist - targetDist;
    float acc = 0;
    float maxAcc = 180;

    if (diff > innerThresh) { // object further than target threshold
        if (diff > outerThresh) diff = outerThresh;
        acc = mapFPN(diff, innerThresh, outerThresh, 0, maxAcc);
        
    } else if (diff < -innerThresh) { // target closer than target threshold
        if (diff < -outerThresh) diff = -outerThresh;
        acc = mapFPN(diff, -innerThresh, -outerThresh, 0, -maxAcc);
    }
    
    carAcc = acc;
    // apply speed changes
    if ( (minDist >= stopThreshold_upper) || (minDist <= stopThreshold_lower) ) {  // stop car if object too far away or too close
        carSpeed = 0;

    } else {
        carSpeed += acc / FPS;
        if (carSpeed > topSpeed) carSpeed = topSpeed;
        if (carSpeed < 0) carSpeed = 0;
    }

    // see if need to turn
    int turnAmount = 100;
    if (maxDist > 1.5*minDist){
        
        if (leftDist > rightDist){
            turn = -turnAmount;
        } else {
            turn = turnAmount;
        }
    } else {
        turn = 0;
    }
}

// improved function to update the speed
void updateSpeedImproved(float left_values[], float right_values[]) {

    const float minDist = min(left_values[0], right_values[0]);
    const float maxDist = max(left_values[0], right_values[0]);

    const float stopThreshold_upper = 40;
    const float stopThreshold_lower = 3;

    float targetDist = 16.0;
    int topSpeed = 140;
    float conversion_coef = 200.0 / 180.0; // conversion between value written to motors and real life speed in cm/s
    int prev_i = 3; // index for previous reading to compare to current reading
    float T = 0.25; // time where theoretical line intersects target velocity line
    float m1 = carSpeed * conversion_coef;
    
    float m3, targetSpeed, rel_vel; // velocity to acheive / velocity converted to motor speed
    float d1 = 0, d0 = 0, delta_t; // current min distance / some previous min distance / time in sec between two readings

    if ( (minDist >= stopThreshold_upper) || (minDist <= stopThreshold_lower) ) {  // stop car if object too far away or too close
        carSpeed = 0;

    } else {
        d1 = minDist - targetDist;
        d0 = min(left_values[prev_i], right_values[prev_i]) - targetDist;
        delta_t = (float)(prev_i * switchDelay * 2) / 1000;

        float targetVelMult = 1;
        rel_vel = (d1 - d0) / delta_t;
        float rel_vel_limit = -1;
        //if (rel_vel < rel_vel_limit) rel_vel = rel_vel_limit;
        
        m3 = targetVelMult * rel_vel + m1 + d1 / T;
        targetSpeed = m3 / conversion_coef;

        float maxAcceleration = 80 / FPS;
        float maxDeceleration = - 80 / FPS;
        float diff = targetSpeed - carSpeed;

        //if (diff > maxAcceleration) diff = maxAcceleration;
        //if (diff < maxDeceleration) diff = maxDeceleration;
        
        carSpeed += diff * 0.1;
        if (carSpeed > topSpeed) carSpeed = topSpeed;
        if (carSpeed < 0)        carSpeed = 0;
    }

    if (false){
        //Serial.print("carSpeed: ");
        //Serial.print(carSpeed);
        //Serial.print(" m1: ");
        Serial.print(rel_vel);
        Serial.print(" ");
        //Serial.print(" d1: ");
        Serial.print(targetSpeed/10);
        //Serial.print(" d0: ");
        //Serial.print(d0);
        //Serial.print(" r_vel: ");
        Serial.print(" ");
        Serial.print(d1);
        Serial.print(" ");
        //Serial.print(d0);
        Serial.print(" ");
        //Serial.print(" m3: ");
        Serial.print(carSpeed/10);
        //Serial.print(" t_speed: ");
        //Serial.print(targetSpeed);
        Serial.print(" ");
        Serial.println();
    }
}

// function to update motors without having to think about analogWrite()
// Takes in carSpeed and turn variables and writes analog values to
// variables leftMotorPin, rightMotorpin and reversePin
void updateMotors(int carSpeed, int turn){
    
  int turnHold, rightSpeed, leftSpeed, motorSpeed;
  if (carSpeed == 0)        motorSpeed = 0;
  else if (carSpeed > 0)    motorSpeed = map(carSpeed, 0, 255, MIN_SPEED, 255);
  else if (carSpeed < 0)    motorSpeed = map(carSpeed, 0, -255, -MIN_SPEED, -255);
  
  if ( (turn == 100) || (turn == -100) ) motorSpeed *= 1;
  
  if (carSpeed < 0){ // reverse
    analogWrite(leftMotorPin, 0);
    analogWrite(rightMotorPin, 0);
    analogWrite(reversePin, -motorSpeed);
  
  } else { // go forward with some turn
    if (turn == 0){         // go straight
      rightSpeed = motorSpeed;
      leftSpeed = motorSpeed;
    } else if (turn < 0){   // turn right
      turnHold = turn * -1;
      rightSpeed = motorSpeed;
      leftSpeed = motorSpeed * (100 - turnHold) / 100;
    } else {                // turn left
      leftSpeed = motorSpeed;
      rightSpeed = motorSpeed * (100 - turn) / 100;
    }
    analogWrite(leftMotorPin, rightSpeed);
    analogWrite(rightMotorPin, leftSpeed);
    analogWrite(reversePin, 0);
    
    /*Serial.print(turn);
    Serial.print(" ");
    Serial.print(rightSpeed);
    Serial.print(" ");
    Serial.println(leftSpeed);*/
  }
}

// Function for reading distance with ultrasonic sensor. 
// Returns distance in cm
// Give trig and echo pin numbers as array
float getDistance(int pins[]){
    
    int tPin = pins[0];
    int ePin = pins[1];
    long duration;
    float distance;
    
    digitalWrite(tPin, LOW);
    delayMicroseconds(2);
    digitalWrite(tPin, HIGH);
    delayMicroseconds(10);      // high signal for 10 micro seconds
    digitalWrite(tPin, LOW);
    
    duration = pulseIn(ePin, HIGH);
    distance = duration * 340.0 / 10000 / 2;  // converts time [in microseconds] to distance [in cm]
    return distance;
}

// updates leds based on state of car
void updateLeds(){
    if (!carIsActive){ // standby mode
        digitalWrite(redLedPin, HIGH);
        digitalWrite(greenLedPin, HIGH);
    } else if (carSpeed > 0) { // car moving forward
        digitalWrite(redLedPin, LOW);
        digitalWrite(greenLedPin, HIGH);
    } else if (carSpeed == 0) { // car stopped
        digitalWrite(redLedPin, HIGH);
        digitalWrite(greenLedPin, LOW);
    } else { // car is reversing, blink red led
        digitalWrite(greenLedPin, LOW);
        if ( (millis() - lastBlinkTime) > blinkDelay) {
            digitalWrite(redLedPin, !digitalRead(redLedPin));
            lastBlinkTime = millis();
        }
    }
}

//// HELPER FUNCTIONS ////

// custom map float funciton
float mapFPN(float n, float a1, float a2, float b1, float b2) {
    
    float ans = n;
    float span1 = a2 - a1;
    float span2 = b2 - b1;
    
    ans -= a1;
    ans *= span2 / span1;
    ans += b1;
    
    return ans;
}

// optional addToArray function
void addToFloatArray(float value, float arr[], int len){
    
    for (int i = 9; i > 0; i--){
        arr[i] = arr[i-1];
    }
    arr[0] = value;
}

//// INTERRUPT FUNCTIONS ////

// function to handle interrupt debouncing. Interrupt will have no
// effect if it happens within 300ms of last interrupt. 
void handleInterrupt(){
  if (interruptHappened){
    if ( (millis() - lastDebounceTime) > debounceDelay){
      if (carIsActive) { // car on standby
        carIsActive = false;
        carSpeed = 0;
        //Serial.println("Companion car is on standby mode");
      } else {
        carIsActive = true;
      }
      lastDebounceTime = millis();
    }
    interruptHappened = false;
  }
}

// Interrupt function attached to pin 2
void ISR_button(){
  interruptHappened = true;
}

//// PRINTING FUNCTIONS ////

void printDistances(){
  Serial.print("left: ");
  Serial.print(distanceLeft);
  Serial.print(" right: ");
  Serial.println(distanceRight);
}

// For serial plotting
void plotDistanceReadings(){
    Serial.print(distanceLeft);
    Serial.print(" ");
    Serial.println(distanceRight);
}


//// TESTING FUNCTIONS ////

// serial plots data from US sensors
void testSensors(){
    int leftDist = -1, rightDist = -1;

    if ( (millis() - lastSensorSwitch) > 50){
        sensingRight = (sensingRight == false);
        if (sensingRight){
            rightDist = getDistance(US_right);
            if (distanceRight > DIST_CAP) distanceRight = DIST_CAP;
        } else {
            leftDist = getDistance(US_left);
            if (distanceLeft > DIST_CAP) distanceLeft = DIST_CAP;
            plotDistanceReadings();
        }
        lastSensorSwitch = millis();
    }
}
