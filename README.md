# Companion Car
Group project for our Digital Fabrication course. Companion Car is a car that will follow anybody who gets too close. For now we will only implement it to follow an object, not a person as it is an whole other story to make it follow an object.

## Description
Our project is a four-wheeler that follows an object in a certain proximity using ultra sonic sensors. When the object moves to either left or right, the car will turn in that direction and continue following it.

## TinkerCad
https://www.tinkercad.com/things/03wZLH3eO50-digifab-project-code-sketch/editel?sharecode=uSzYV2oTTXiJcjsT5kaqQSpBs2AQgAE4hGEIf-eezg8

## Usage
A companion car/bot that follows a chosen object everywhere

## Roadmap
- Project finished by 3.5.2022

## Authors and acknowledgment
Markus Teuhola
Nhan Nguyen Duc
Aleksi Talman
Matt Stirling

## Project status
Project is finished.
